#!/usr/bin/env python

import xml.etree.ElementTree as ET
import urllib.request
import ssl 

url = input("Enter URL: ")
if len(url) is 0: 
    url = "http://py4e-data.dr-chuck.net/comments_158792.xml"

# Ignore SSL certificate errors 
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

fhandle = urllib.request.urlopen(url, context=ctx)
print("Retrieving:", url)
data = fhandle.read()
print('Retrieved', len(data), 'characters')
print(data.decode()) #this prints the XML-file #great!

tree = ET.fromstring(data) #now we have a tree with all the nodes in it.

sum = 0
count = 0

for comment in tree.iter('comment') :
    #print(comment.find('name').text, comment.find('comment').text) 
    count = count + 1
    print(comment[0].text, comment[1].text)
    number = int(comment[1].text)
    sum = sum + number

print("Count:", count)
print("Sum:", sum)
